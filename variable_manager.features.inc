<?php

/**
 * Implementation of hook_features_export_options. [component_hook]
 *
 * This hook will alert features of which specific items of this component may
 * be exported. For instances, in this case, we want to make available all the
 * existing items.  If there are no items to be exported, this component will
 * not be made available in the features export page.
 *
 * @return array
 *   A keyed array of items, suitable for use with a FormAPI select or
 *   checkboxes element.
 */
function variable_manager_features_export_options() {
	$options = array();


	$result = db_select('variable_manager', 'vm')
	->fields('vm', array('title', 'machine_name'))
	->execute();
	while($record = $result->fetchAssoc()) {

		$options[$record['machine_name']] = $record['title'];
	}
	//drupal_set_message('<pre>'.print_r($options, 1).'</pre>');

	return $options;
}


/**
 * @param array $data
 *   this is the machine name for the component in question
 * @param array &$export
 *   array of all components to be exported
 * @param string $module_name
 *   The name of the feature module to be generated.
 * @return array
 *   The pipe array of further processors that should be called
 */
function variable_manager_features_export($data, &$export, $module_name = '') {
 
  // fontyourface_default_fonts integration is provided by Features.
  $export['dependencies']['variable_manager'] = 'variable_manager';
 	foreach ($data as $component) {
		$export['features']['variable_manager_config'][$component] = $component;
	}
	return array();
}



function variable_manager_features_export_render($module_name, $data, $export = NULL) {
	$code = array();
	$code[] = '  $variable_manager = array();';
	$code[] = '';
	foreach ($data as $sys_name) {
		
		$item = db_select('variable_manager', 'vm')
    ->condition('vm.machine_name', $sys_name, '=')
    ->fields('vm', array('id','machine_name', 'title', 'description', 'created', 'updated', 'uid'))
    ->execute()
    ->fetchAssoc();
		
		$code[] = '  $variable_manager[] = '. features_var_export($item, '  ') .';';
		
	}
	$code[] = '  return $variable_manager;';
	$code = implode("\n", $code);
	return array('variable_manager_features_default_settings' => $code);
}


function variable_manager_features_rebuild($module) {
	$items = module_invoke($module, 'variable_manager_features_default_settings');
	
	foreach ($items as $idx => $item) {
		
		//$saved = _mymodule_config_set_data($item);
	}
}

/**
 * Implementation of hook_features_revert(). [component_hook]
 */
function variable_manager_features_revert($module) {
	variable_manager_features_rebuild($module);
}